""" websockets.readthedocs.io/en/latest/intro.html#basic-example """

# stackoverflow.com/a/12042843/4453945

import csv
import json
import asyncio

import ai
import websockets




class GeneratedData:
	""" an tagged datatype """
	def __init__(self, is_update, data):
		self.is_update = is_update
		self.data = data
		return



def process_record(raw_dict, doNLP=False):
	if doNLP:
		data = ai.process(raw_dict)
		type_of_change = "addendum"
	else:
		data = raw_dict
		type_of_change = "new data"
	ready_dict = {
		"meatOfData": data,
		"typeOfChange": type_of_change
	}
	return json.dumps(ready_dict)

def record_gen(filename):
	with open(filename, mode='rt', encoding="latin-1") as fh:
		records = list(
			csv.DictReader(fh)
		)
	i = int()
	while True:
		if i < 0:
			break
		try:
			feedback = yield GeneratedData(
				is_update=False,
				data=process_record(records[i])
			)
		except IndexError:
			break
		else:
			try:
				i += int(feedback)
			except ValueError:
				assert feedback == "analyze"
				_ = yield GeneratedData(
					is_update=True,
					data=process_record(records[i], doNLP=True)
				)
	return i

async def ws_server(ws, path):
	data_generator = record_gen('slimmed_realest_data.csv')
	human_feedback = None
	while True:
		try:
			piece = data_generator.send(human_feedback)
			if piece.is_update:
				_ = data_generator.send(None)									# ignore effect of next 1 yield as described in `bug`
		except StopIteration as e:
			generator_return = e.value
			break
		else:
			await ws.send(piece.data)
			human_feedback = await ws.recv()
	print(generator_return)
	exit(0)	# return 0 doesn't terminate

start_server = websockets.serve(ws_server, "localhost", 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
