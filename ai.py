import string
import random

def make_random_str(minLen, maxLen):
	return "".join(
		[
			random.choice("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
			for _ in range(  random.randint(minLen, maxLen)  )
		]
	)

def process(x):
	table = list()
	for i in range(8):
		row = list()
		for j in range(12):
			row.append(make_random_str(4,12))
		table.append(row)
	return table
