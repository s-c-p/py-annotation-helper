Vue.component('row-cells', {
    props: ['cell'],
    template: '<td>{{  cell.text  }}</td>'
});

Vue.component('table-row', {
    props: ['row'],
    template: '<tr><row-cells v-for="cell in row.cells" v-bind:cell="cell" v-bind:key="cell.id"></row-cells></tr>'
});




let pyArrayToVueArray = function (arrOfarrOfStr) {
    // really gotta improve this,, can I use map here?
    let row = new Array();
    let vueArray = new Array();
    for (let r = 0; r < arrOfarrOfStr.length; r++) {
        vueArray.push({
            "id": `_${r}`,
            "cells": new Array()
        });
        row = arrOfarrOfStr[r];
        for (let c = 0; c < row.length; c++) {
            vueArray[r]
            .cells.push({
                "id": `_${r}_${c}`,
                "text": row[c]
            });
        }
    }
    return vueArray;
}

let talker = function (obj_ref, wsUrl) {
	// mostly copy pasted from trunklucator's reconnecting-websocket
    var timer_id;
    var reconnectAttempts = 0;
    const reconnectDecay = 1.5;
    const reconnectInterval = 3000;
    const maxReconnectInterval = 60000;

    function connect() {
        obj_ref.ws = new WebSocket(wsUrl);

        //Error callback
        obj_ref.ws.onerror = function(wsEvent) {
            console.error("error!", wsEvent.reason);
            obj_ref.setStatus('Not connected.')
            obj_ref.opened = false;
        };

        //socket opened callback
        obj_ref.ws.onopen = function(wsEvent) {
            clearTimeout(timer_id)
            obj_ref.setStatus('Connected.')
            obj_ref.opened = true;
        };

        //message received callback
        obj_ref.ws.onmessage = function (wsEvent) {
            let json_data = JSON.parse(wsEvent.data)
            if (json_data.typeOfChange === "new data") {
                obj_ref.fetched_data = JSON.stringify(json_data.meatOfData, null, 4);
                obj_ref.show_analysis = false;
                obj_ref.analysis = {};
            } else if (json_data.typeOfChange === "addendum") {
                obj_ref.show_analysis = true;
                obj_ref.analysis = pyArrayToVueArray(json_data.meatOfData);
            } else {
                console.log(wsEvent.data);
                alert("strange typeOfChange received");
            }
            console.log(wsEvent);
        }

        //socket closed callback
        obj_ref.ws.onclose = function(wsEvent) {
            clearTimeout(timer_id);
            obj_ref.opened = false;
            var timeout = reconnectInterval * Math.pow(reconnectDecay, reconnectAttempts);
            timeout = timeout > maxReconnectInterval ? maxReconnectInterval : timeout;
            console.log('Socket is closed. Reconnect will be attempted in ' + timeout + ' second.', wsEvent);
            obj_ref.setStatus('Reconnecting...');
            timer_id = setTimeout(function() {
                reconnectAttempts++;
                connect();
            }, timeout);
        };

        //when browser window closed, close the socket, to prevent server exception
        window.onbeforeunload = function() {
            obj_ref.ws.close();
        };
    };
    connect();
};




const wsVue = new Vue({
    el: '#app',
    data: {
        conn_status: "none",
        fetched_data: {},
        analysis: {},
        show_analysis: false
    },
    methods: {
        getNextChunk: function() {this.ws.send(1)},
        getPrevChunk: function() {this.ws.send(-1)},
        setStatus: function (status) {this.conn_status = status},
        analyze: function() {this.ws.send("analyze")}
    },
    // computed: {},
    created: function () {
        const self = this   // inspired by trunklucator
        talker(self, 'ws://localhost:8765')
    },
});
